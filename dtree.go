package info

type BinDTree interface {
	Decision(params map[int]bool) bool
	Params() []int
	Gain(int) float64
}

type BinNode struct {
	id          int
	information map[int]float64

	left  BinDTree
	right BinDTree

	//remove this
}

func (this *BinNode) Decision(params map[int]bool) bool {
	if params[this.id] {
		return this.right.Decision(params)
	} else {
		return this.left.Decision(params)
	}
}

func (this *BinNode) Params() []int {
	params := []int{this.id}
	params = append(params, this.left.Params()...)
	return append(params, this.right.Params()...)
}

type BinLeaf bool

func (this BinLeaf) Decision(params map[int]bool) bool {
	return bool(this)
}

func (this BinLeaf) Params() []int {
	return []int(nil)
}
