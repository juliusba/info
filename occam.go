package info

func OccamsDTree(trainingSet map[int][]bool, targetHistory []bool,
	minEntropy float64, minSamples int, informationMap map[int]float64) BinDTree {

	if BinEntropy(targetHistory) < minEntropy || len(targetHistory) < minSamples {
		pos := 0
		for _, bin := range targetHistory {
			if bin {
				pos++
			} else {
				pos--
			}
		}
		return BinLeaf(pos >= 0)
	}

	var (
		bestId   int
		bestGain float64
	)

	for id, history := range trainingSet {
		gain := InformationGain(
			ConditionalEntropy(
				JointEntropy(targetHistory, history),
				BinEntropy(history)),
			BinEntropy(targetHistory))

		if gain > bestGain {
			bestGain = gain
			bestId = id
		}
	}

	leftTS := make(map[int][]bool)
	rightTS := make(map[int][]bool)
	for id := range trainingSet {
		if id != bestId {
			leftTS[id] = make([]bool, 0, len(targetHistory))
			rightTS[id] = make([]bool, 0, len(targetHistory))
		}
	}
	leftTH := make([]bool, 0, len(targetHistory))
	rightTH := make([]bool, 0, len(targetHistory))

	for i := 0; i < len(trainingSet[bestId]); i++ {
		if trainingSet[bestId][i] {
			for id := range leftTS {
				leftTS[id] = append(leftTS[id], trainingSet[id][i])
			}
			leftTH = append(leftTH, targetHistory[i])
		} else {
			for id := range leftTS {
				rightTS[id] = append(rightTS[id], trainingSet[id][i])
			}
			rightTH = append(rightTH, targetHistory[i])
		}
	}

	node := new(BinNode)
	node.id = bestId
	node.left = OccamsDTree(leftTS, leftTH, minEntropy, minSamples, informationMap)
	node.right = OccamsDTree(rightTS, rightTH, minEntropy, minSamples, informationMap)

	return node
}
