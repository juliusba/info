package info

import (
	"log"
	"math"
)

type BitStream interface {
	P() float64
	Entropy(tick int) float64
	History() []float64
}

func Entropy(probability float64) float64 {
	entropy := probability*math.Log2(probability) + (1-probability)*math.Log2(1-probability)
	return entropy
}

func BinEntropy(X []bool) float64 {
	var count float64
	for _, val := range X {
		if val {
			count++
		}
	}
	probability := count / float64(len(X))

	return Entropy(probability)
}

func ToBool(X []float64) []bool {
	B := make([]bool, len(X))
	for i, val := range X {
		B[i] = val != 0.0
	}
	return B
}

func JointEntropy(X, Y []bool) float64 {
	if len(X) != len(Y) {
		log.Fatal("entropy.JointEntropy()")
	}

	var (
		pxy, pXy, pxY, pXY float64
	)

	for i := 0; i < len(X); i++ {
		if X[i] {
			if Y[i] {
				pXY++
			} else {
				pXy++
			}
		} else {
			if Y[i] {
				pxY++
			} else {
				pxy++
			}
		}
	}
	pxy /= float64(len(X))
	pXy /= float64(len(X))
	pxY /= float64(len(X))
	pXY /= float64(len(X))

	JointEntropyXY := -(pxy*math.Log2(pxy) + pXy*math.Log2(pXy) + pxY*math.Log2(pxY) + pXY*math.Log2(pXY))

	return JointEntropyXY
}

func ConditionalEntropy(JointEntropyXY, EntropyY float64) float64 {
	return JointEntropyXY - EntropyY
}

func InformationGain(ConditionalEntropyXY, EntropyX float64) float64 {
	return ConditionalEntropyXY - EntropyX
}
